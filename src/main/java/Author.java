public class Author {

    String surname;
    String nationality;

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return "Author{" +
                "surname='" + surname + '\'' +
                ", nationality='" + nationality + '\'' +
                '}';
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Author(String surname, String nationality) {
        this.surname = surname;
        this.nationality = nationality;
    }
}
