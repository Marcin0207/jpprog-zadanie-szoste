import java.lang.reflect.AccessibleObject;

public class Main {

    public static void main(String[] args) {

        Poem[] poemtab = new Poem[3];

        Author poet1 = new Author("Shakespeare","England");
        Author poet2 = new Author("Alighieri","Italy");
        Author poet3 = new Author("Białoszewwski","Poland");

        Poem poem1 = new Poem(poet1,23);
        Poem poem2 = new Poem(poet2,14);
        Poem poem3 = new Poem(poet3,55);

        poemtab[0]=poem1;
        poemtab[1]=poem2;
        poemtab[2]=poem3;

        int highest =0;
        Author bestAuthor = null;
        for (int i = 0; i < poemtab.length; i++) {

            if(poemtab[i].getStropheNumbers() > highest)
            highest = poemtab[i].getStropheNumbers();
            bestAuthor = poemtab[i].getCreator();


        }

        System.out.println("Best Author:"+bestAuthor);





    }

}
